<?php

use Faker\Generator as Faker;

$factory->define(App\Recipient::class, function (Faker $faker) {
    return [
        'email' => $faker->email,
        'activation_token' => str_random(60),
    ];
});
