<?php

use App\Recipient;
use Illuminate\Database\Seeder;

class RecipientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Recipient::truncate();
        // $faker = \Faker\Factory::create();

        // for ($i=0; $i < 10; $i++) { 
        //     Recipient::create([
        //         'email' => $faker->email,
        //     ]);
        // } 
    }
}
