<?php

namespace Tests\Feature;

use App\Recipient;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RegisterTest extends TestCase
{
    public function testsRegistersSuccessfully()
    {
        $payload = [
            'email' => 'john@toptal.com',
        ];

        $this->json('post', '/api/newsletter/register', $payload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'email',
                    'created_at',
                ],
            ]);;
    }

    public function testsRequiresEmail()
    {
        $this->json('post', '/api/newsletter/register')
            ->assertStatus(422)
            ->assertJson([
                'errors' => [
                    'email' => [ 'The email field is required.' ],
                ]
            ]);
    }

    public function testsActivatesSuccessfully(){
        $activation_token = str_random(60);
        $recipient = factory(Recipient::class)->create([
            'email' => 'email@test.com',
            'activation_token' => $activation_token
        ]);

        $response = $this->json('GET', 'api/newsletter/activate/' .$activation_token)
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'email',
                    'created_at',
                ],
            ]);;
    }
    
}
