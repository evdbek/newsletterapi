<?php

namespace App\Http\Controllers;

use App\Recipient;
use Illuminate\Http\Request;
use App\Notifications\SubscribeActivate;

class RecipientController extends Controller
{
    
    public function index() {
        return Recipient::all();
    }

    public function store(Request $request){
        
        $validatedData = $request->validate([
            'email' => 'required|email|max:255|unique:recipients'
        ]);

        $recipient = Recipient::firstOrNew([
            'email' => $request->email,
            'activation_token' => str_random(60),
        ]);

        $recipient->save();
        $recipient->notify(new SubscribeActivate($recipient));

        return response()->json([
            'data' => $recipient->toArray(),
        ]);
    }

    public function subscribeActivate($token) {
       
        $recipient = Recipient::where('activation_token', $token)->first();

        if (!$recipient) {
            return response()->json([
                'message' => 'Invalid activation token'
            ], 404);
        }

        $recipient->active = true;
        $recipient->activation_token = '';
        $recipient->save();

        return response()->json([
            'data' => $recipient->toArray(),
        ]);
    }

}
