<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Recipient extends Model
{

    use Notifiable;

    protected $fillable = ['email', 'active', 'activation_token'];

    protected $hidden = [
        'updated_at',
        'activation_token',
        'id',
    ];
}
