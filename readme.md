## Example Laravel API

 This is a sample project that provides an API that accepts an email-address and persists it inside a database. 

The API provides two endpoints
```
/api/newsletter/register/
/api/newsletter/activate/{token}
```

Upon registration, an email containing the verification link is sent and after the user visits the link the mail record marked as verified. 

## Running the API

It's very simple to get the API up and running. First, create the database (and database
user if necessary) and add them to the `.env` file.

```
DB_DATABASE=your_db_name
DB_USERNAME=your_db_user
DB_PASSWORD=your_password
```
To get the mail sent and monitored, configure also the mail server credentials

```
MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=465
MAIL_USERNAME=username
MAIL_PASSWORD=password
```

Then install, migrate:

1. `composer install`
2. `php artisan migrate`
3. `php artisan serve`

The API will be running on `localhost:8000`.


In order to test, run:

```
composer test
```
